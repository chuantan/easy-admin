package com.mars.module.system.controller;

import com.mars.common.response.sys.CommonDataResponse;
import com.mars.common.result.R;
import com.mars.module.system.service.ICommonDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangqiang
 * @version 1.0
 * @date 2021/3/6 22:15
 */
@RestController
@RequestMapping("/captcha")
@Api(tags = "系统公共接口接口")
@AllArgsConstructor
public class SysCommonDataController {

    private final ICommonDataService captchaService;


    /**
     * 获取系统公共数据
     */
    @ApiOperation(value = "获取系统公共数据")
    @GetMapping("/acquire")
    public R<CommonDataResponse> acquire() {
        return R.success(captchaService.acquire());
    }
}
