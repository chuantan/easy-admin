package com.mars.common.enums;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-15 16:45:35
 */
@SuppressWarnings("all")
public enum FieldTypeEnums {

    /**
     * VARCHAR
     */
    VARCHAR("STRING", "varchar"),
    /**
     * BIGDECIMAL
     */
    BIGDECIMAL("BIGDECIMAL", "decimal"),
    /**
     * INT
     */
    INT("INT", "int"),
    /**
     *TINYINT
     */
    TINYINT("TINYINT", "tinyint"),

    /**
     * BIGINT
     */
    BIGINT("BIGINT", "bigint"),

    /**
     * TEXT
     */
    TEXT("TEXT", "text"),
    /**
     * TEXTAREA
     */
    TEXTAREA("TEXTAREA", "varchar"),
    /**
     * LOCALDATETIME
     */
    LOCALDATETIME("LOCALDATETIME", "datetime");


    private String type;

    private String dbType;


    FieldTypeEnums(java.lang.String type, java.lang.String dbType) {
        this.type = type;
        this.dbType = dbType;
    }

    /**
     * 通过类型获取dbType
     *
     * @param type 类型
     * @return String
     */
    public static String getDbTypeByType(String type) {
        FieldTypeEnums[] fieldTypeEnums = FieldTypeEnums.values();
        for (FieldTypeEnums fieldTypeEnum : fieldTypeEnums) {
            if (fieldTypeEnum.getType().equalsIgnoreCase(type)) {
                return fieldTypeEnum.getDbType();
            }
        }
        return null;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }
}
